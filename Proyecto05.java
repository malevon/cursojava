/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto04;

/**
 *
 * @author malevo
 */
public class Proyecto05 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        irAlChino("vino", "cerveza");
    }
    public static void irAlChino(String producto1, String producto2){
        //CamelCase
        //convencion de nombrar un metodo
        //la clase siempre va a comenzar con mayuscula
        //el metodo nunca va a comenzar con mayuscula
        // metodo irAlChino
        // argumento vino
        //parametro producto
        // si le quiero pasar mas de un producto en lista o arrays
        //
        System.out.println("Voy hasta la puerta\n"
        + "hago 20 metros\n"
        +"cruzola California\n"
        +"cruzo Goncalves Dias"
        +"camino 50 metros y compro:\n"
        + producto1 + "\n"
        + producto2
        );
    }
    
}