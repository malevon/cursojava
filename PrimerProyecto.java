/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primerproyecto;

/**
 *
 * @author malevo
 */
public class PrimerProyecto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int base;
        double alt;
        double area;
        
        double ventanalt;
        double ventanabase;
        double puertaalt;
        double puertabase;
        double altpared;
        double altbase;
        
        base = 10;
        alt = 1.9;
        area = base*alt;
        
        ventanalt = 1;
        ventanabase = 1.7;
        puertaalt  = 1.84;
        puertabase = 0.5;
        altpared = 2.5;
        altbase = 10;
        
        System.out.print("El Resultado de la base por la altura es:  ");
        System.out.println(base*alt);
        System.out.print("El Resultado de la base por la altura en una sola variable:  ");
        System.out.println(area);
        System.out.println();
        
        System.out.print("El Resultado area del area de la ventana:  ");
        System.out.println(ventanalt * ventanabase);
        System.out.print("El Resultado area de la suma de las dos ventanas:  ");
        System.out.println(ventanalt * ventanabase * 2);
        System.out.print("El Resultado area del area de la puerta:  ");
        System.out.println(puertaalt * puertabase);
        System.out.print("El Resultado area del area de la pared:  ");
        System.out.println(altpared * altbase);
        System.out.print("Sumo las dos ventanas mas la puerta y resto la pared:  ");
        System.out.println((altpared*altbase)-(ventanalt*ventanabase*2)-(puertaalt*puertabase));
    }
    
}
